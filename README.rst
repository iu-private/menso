Personal configuration repository
=================================

This repository is just another (4th?) attempt to bring sanity to
management of my scripts, configuration files, repositories and
development environment in general.

Using configuration management system allows (at least, in theory)
install all needed packages, deploy all needed configuration files and
setup system in proper way.

Cdist has concept of :bold:`types`, representing unit of
functionality.  Names of all types must start with two underscores
(__), such convention is enforced in Cdist sources.

Names of types that are definitely too personal and have no
perspectives of having general-purpose usefulness also ends with
underscore.

This configuration is compatible with cdist-4.4.1 (version in Debian
sid) and is probably forward-compatible.
