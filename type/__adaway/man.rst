cdist-type__adaway(7)
=====================

NAME
----

cdist-type__adaway - reconfigure Debian package

DESCRIPTION
-----------

This singleton type blocks huge list of advetisement/tracking
hosts by modifying */etc/hosts*. It is inspired by AdAway project
and uses its list of hosts.

Note: list of advertisement is downloaded from Internet, and so it is
volatile. It is recommended to run this type once a week or so.

REQUIRED PARAMETERS
-------------------

None.

OPTIONAL PARAMETERS
-------------------

None.

MESSAGES
--------

None.

EXAMPLES
--------

.. code-block:: sh

    __adaway
   
SEE ALSO
--------

:strong:`hosts`\ (5)

COPYING
-------

Copyright (C) 2018 Dmitry Bogatov. Free use of this software is granted
under the terms of the GNU General Public License version 3 or later
(GPLv3+).
