syntax on
highlight PreProc ctermfg=26
set background=dark
"let g:solarized_contrast = "low"
"colorscheme solarized
call plug#begin('~/.vim/plugged')
  Plug 'Shougo/neosnippet' | Plug 'Shougo/neosnippet-snippets' 
  Plug 'honza/vim-snippets'
  Plug 'scrooloose/nerdtree'
  Plug 'neovimhaskell/haskell-vim'
call plug#end()
let g:neosnippet#snippets_directory=[]
let g:neosnippet#snippets_directory+=['~/.vim/plugged/neosnippet-snippets/neosnippets'] 
let g:neosnippet#snippets_directory+=['~/.vim/plugged/vim-snippets/snippets'] 
let g:neosnippet#snippets_directory+=['~/.config/nvim/snippets']
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)
nnoremap <C-N> :NERDTreeToggle<CR>
nnoremap <C-n> :NERDTreeFocus<CR>

let NERDTreeChDirMode=2
let NERDTreeWinSize=25
