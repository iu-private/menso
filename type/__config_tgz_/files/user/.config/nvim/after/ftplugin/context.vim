setlocal expandtab
setlocal shiftwidth=2
nnoremap z{ bhi{<ESC>eea}<ESC>
inoremap --- <BS>{\emdash}
inoremap @. \cdot{}
setlocal equalprg=alignrx\ --tex

