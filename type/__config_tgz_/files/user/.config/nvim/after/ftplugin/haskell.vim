setlocal equalprg=stylish-haskell
setlocal expandtab
setlocal shiftwidth=2
setlocal makeprg=aux\ typecheck
