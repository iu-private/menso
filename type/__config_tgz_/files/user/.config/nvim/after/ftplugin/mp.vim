setlocal expandtab
setlocal nocopyindent
setlocal nosmartindent
setlocal shiftwidth=2
filetype indent off
setlocal iskeyword+=.,#
nnoremap <buffer> <F10> :r!cat ~/.config/nvim/text/any/gpl3\|sed 's/^/\%\% /'<Return>
