highlight shOption ctermfg=209
highlight shStatement ctermfg=196
highlight shQuote ctermfg=140
highlight shEcho ctermfg=171
highlight shIf ctermfg=80
highlight shFor ctermfg=69
highlight shSingleQuote ctermfg=112
highlight shDoubleQuote ctermfg=93
highlight shFunction ctermfg=22
highlight shRedir ctermfg=10
highlight shCommandSub ctermfg=88
highlight shPosnParm ctermfg=54
highlight shCase ctermfg=61
highlight shTouch ctermfg=56
highlight shSpecial ctermfg=214
highlight shShellVariables ctermfg=26

highlight bashSpecialVariables ctermfg=255
highlight shHereDoc ctermfg=130

syntax keyword Keyword readonly

syntax keyword shSpecial __apt_key
syntax keyword shSpecial __apt_key_uri
syntax keyword shSpecial __apt_mark
syntax keyword shSpecial __apt_norecommends
syntax keyword shSpecial __apt_ppa
syntax keyword shSpecial __apt_source
syntax keyword shSpecial __apt_update_index
syntax keyword shSpecial __block
syntax keyword shSpecial __ccollect_source
syntax keyword shSpecial __cdist
syntax keyword shSpecial __cdistmarker
syntax keyword shSpecial __chroot_mount
syntax keyword shSpecial __chroot_umount
syntax keyword shSpecial __config_file
syntax keyword shSpecial __consul
syntax keyword shSpecial __consul_agent
syntax keyword shSpecial __consul_check
syntax keyword shSpecial __consul_reload
syntax keyword shSpecial __consul_service
syntax keyword shSpecial __consul_template
syntax keyword shSpecial __consul_template_template
syntax keyword shSpecial __consul_watch_checks
syntax keyword shSpecial __consul_watch_event
syntax keyword shSpecial __consul_watch_key
syntax keyword shSpecial __consul_watch_keyprefix
syntax keyword shSpecial __consul_watch_nodes
syntax keyword shSpecial __consul_watch_service
syntax keyword shSpecial __consul_watch_services
syntax keyword shSpecial __cron
syntax keyword shSpecial __debconf_set_selections
syntax keyword shSpecial __directory
syntax keyword shSpecial __docker
syntax keyword shSpecial __docker_compose
syntax keyword shSpecial __dog_vdi
syntax keyword shSpecial __file
syntax keyword shSpecial __filesystem
syntax keyword shSpecial __firewalld_rule
syntax keyword shSpecial __firewalld_start
syntax keyword shSpecial __git
syntax keyword shSpecial __group
syntax keyword shSpecial __hostname
syntax keyword shSpecial __install_bootloader_grub
syntax keyword shSpecial __install_chroot_mount
syntax keyword shSpecial __install_chroot_umount
syntax keyword shSpecial __install_config
syntax keyword shSpecial __install_file
syntax keyword shSpecial __install_fstab
syntax keyword shSpecial __install_generate_fstab
syntax keyword shSpecial __install_mkfs
syntax keyword shSpecial __install_mount
syntax keyword shSpecial __install_partition_msdos
syntax keyword shSpecial __install_partition_msdos_apply
syntax keyword shSpecial __install_reboot
syntax keyword shSpecial __install_reset_disk
syntax keyword shSpecial __install_stage
syntax keyword shSpecial __install_umount
syntax keyword shSpecial __iptables_apply
syntax keyword shSpecial __iptables_rule
syntax keyword shSpecial __issue
syntax keyword shSpecial __jail
syntax keyword shSpecial __jail_freebsd10
syntax keyword shSpecial __jail_freebsd9
syntax keyword shSpecial __key_value
syntax keyword shSpecial __keyboard
syntax keyword shSpecial __line
syntax keyword shSpecial __link
syntax keyword shSpecial __locale
syntax keyword shSpecial __locale_system
syntax keyword shSpecial __motd
syntax keyword shSpecial __mount
syntax keyword shSpecial __mysql_database
syntax keyword shSpecial __package
syntax keyword shSpecial __package_apt
syntax keyword shSpecial __package_dpkg
syntax keyword shSpecial __package_emerge
syntax keyword shSpecial __package_emerge_dependencies
syntax keyword shSpecial __package_luarocks
syntax keyword shSpecial __package_opkg
syntax keyword shSpecial __package_pacman
syntax keyword shSpecial __package_pip
syntax keyword shSpecial __package_pkg_freebsd
syntax keyword shSpecial __package_pkg_openbsd
syntax keyword shSpecial __package_pkgng_freebsd
syntax keyword shSpecial __package_rubygem
syntax keyword shSpecial __package_update_index
syntax keyword shSpecial __package_upgrade_all
syntax keyword shSpecial __package_yum
syntax keyword shSpecial __package_zypper
syntax keyword shSpecial __pacman_conf
syntax keyword shSpecial __pacman_conf_integrate
syntax keyword shSpecial __pf_apply
syntax keyword shSpecial __pf_ruleset
syntax keyword shSpecial __postfix
syntax keyword shSpecial __postfix_master
syntax keyword shSpecial __postfix_postconf
syntax keyword shSpecial __postfix_postmap
syntax keyword shSpecial __postfix_reload
syntax keyword shSpecial __postgres_database
syntax keyword shSpecial __postgres_extension
syntax keyword shSpecial __postgres_role
syntax keyword shSpecial __process
syntax keyword shSpecial __pyvenv
syntax keyword shSpecial __qemu_img
syntax keyword shSpecial __rbenv
syntax keyword shSpecial __rsync
syntax keyword shSpecial __rvm
syntax keyword shSpecial __rvm_gem
syntax keyword shSpecial __rvm_gemset
syntax keyword shSpecial __rvm_ruby
syntax keyword shSpecial __ssh_authorized_key
syntax keyword shSpecial __ssh_authorized_keys
syntax keyword shSpecial __ssh_dot_ssh
syntax keyword shSpecial __staged_file
syntax keyword shSpecial __start_on_boot
syntax keyword shSpecial __sysctl
syntax keyword shSpecial __timezone
syntax keyword shSpecial __update_alternatives
syntax keyword shSpecial __user
syntax keyword shSpecial __user_groups
syntax keyword shSpecial __yum_repo
syntax keyword shSpecial __zypper_repo
syntax keyword shSpecial __zypper_service
