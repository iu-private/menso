" WTF? None of out-of-box color themes do not render comments in green.
autocmd! VimEnter * :colorscheme delek

set rtp+=/usr/share/vim/addons
command! Cfg :tabnew ~/.config/nvim/init.vim
command! -range=% DelWS :<line1>,<line2>s/ $//
command! Ru :set keymap=russian-jcukenwin
command! Eo :set keymap=esperanto
command! Sign :r!date '+ -- Dmitry Bogatov <KAction@gnu.org>  \%a, \%02d \%3b \%Y \%T \%z'
" change keymap
inoremap <C-\> <C-^>

nnoremap z/ :tabnew term://dict\ <cword><Return>
runtime plugin/man.vim

" Spell checking is useful, but knows nothing about technical
" jargon, making it annoying, when enabled by default. Use
"     :setlocal spell
" to enable it.
set spelllang=en
set spellfile=~/.cache/nvim-spell.utf-8.add

set autoindent
set cmdwinheight=4
set copyindent
set exrc
set secure
"tag completion
set complete+=t
set foldclose=all
set foldmethod=marker
set incsearch
set magic
set smartcase
set number
set nohlsearch
set lisp
set noloadplugins
set ruler
set textwidth=72
" Vim thinks, that /bin/sh scripts must use backticks for command
" substitution. Make it believe that shell is actually bash.
let g:is_bash=1
let g:sh_fold_enabled=7
set iskeyword=a-z,A-Z,_,.,39
set foldlevel=99
au BufWrite * :silent !aux make-tags &

au VimEnter * source ~/.config/nvim/after/enter.vim
" vim: nospell
