#!/bin/sh -eu
# Copyright (C) 2016 Dmitry Bogatov <KAction@gnu.org>

# Author: Dmitry Bogatov <KAction@gnu.org>

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
set -eu

. ~/.local/share/posix/color.sh

# Here we trust ${HOSTNAME} from environment, but not ${PWD}, due
# following reasons:
#
#  * hostname rarely changes, so if something set it, it is probably
#    still valid. On other hand, rc(1) shell can easily inherit PWD
#    from posix shell and neither update, nor unset it.
#
#  * `pwd` is builtit, `hostname` is external process
#
[ "${HOSTNAME:-}" ] || HOSTNAME=$(hostname)
PWD=$(pwd)
ERRNO=${1}

[ -n "${SHLVL:-}" ] && color --yellow "($SHLVL)"
color --xblack "${HOSTNAME}|"


if gitroot="$(git rev-parse --show-toplevel 2> /dev/null)" ; then
	if [ "$(basename "$gitroot")" = debian ] ; then
		display_root=$(basename "$(dirname "$gitroot")")/debian
	else
		display_root=$(basename "$gitroot")
	fi
	relrepo=$(echo -n "${PWD}" | sed -e "s:$gitroot::")

	color --cyan { --yellow "${display_root}" --white "${relrepo}" --cyan }
fi
unset gitroot display_root relrepo

pretty_pwd=$(echo "$PWD" | sed -e 's#$#/#' -e "s#$HOME/#~/#" -e 's#//$#/#')
colorspec='--xred'
[ -w "${PWD}" ] && colorspec='--xblue'
color "$colorspec" "$pretty_pwd" --xgreen "$(date '+ %b %d %H:%M')"
unset colorspec pretty_pwd
echo

colorspec='--white'
[ "$ERRNO" != 0 ] && colorspec='--red'
color "${colorspec}" '.. '
unset colorspec ERRNO

# Required for using GPG auth key instead of ssh.
gpg-connect-agent updatestartuptty /bye > /dev/null 2>/dev/null
