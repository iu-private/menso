# Copyright (C) 2017 Dmitry Bogatov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
. ~/.local/share/posix/color-list.sh

color ()
{
    for arg ; do
	case "$arg" in
	    --black) arg=${COLOR_BLACK} ;;
	    --xblack) arg=${COLOR_xBLACK} ;;
	    --white) arg=${COLOR_WHITE} ;;
	    --xwhite) arg=${COLOR_xWHITE} ;;
	    --red) arg=${COLOR_RED} ;;
	    --xred) arg=${COLOR_xRED} ;;
	    --green) arg=${COLOR_GREEN} ;;
	    --xgreen) arg=${COLOR_xGREEN} ;;
	    --yellow) arg=${COLOR_YELLOW} ;;
	    --xyellow) arg=${COLOR_xYELLOW} ;;
	    --blue) arg=${COLOR_BLUE} ;;
	    --xblue) arg=${COLOR_xBLUE} ;;
	    --magenta) arg=${COLOR_MAGENTA} ;;
	    --xmagenta) arg=${COLOR_xMAGENTA} ;;
	    --cyan) arg=${COLOR_CYAN} ;;
	    --xcyan) arg=${COLOR_xCYAN} ;;
	    --white) arg=${COLOR_WHITE} ;;
	    --xwhite) arg=${COLOR_xWHITE} ;;
	    *) ;;
	esac
	echo -n "$arg"
    done
    echo -n "$COLOR_RESET"
}
