# Copyright (C) 2017 Dmitry Bogatov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Check if we are online. If we are not, functions below may use
# $USBROOT instead.
if ping -c1 -w1 gnu.org >/dev/null 2>/dev/null ; then
    online () { true; }
else
    online () { false; }
fi

USBROOT=/mnt/viking/value/git

if [ -d "${USBROOT}" ]; then
    usb_mounted () { true; }
else
    usb_mounted () { false; }
fi

alioth_uri ()
{
    local section name user
    section=$1; name=$2; user=kaction-guest;
    echo https://anonscm.debian.org/cgit/users/${user}/${section}/${name}.git
}

git_own_ensure ()
{
    local section name
    section=$1; name=$2;
    if usb_mounted ; then
	repo=${USBROOT}/${name}.git
	if [ ! -d "${repo}" ] ; then
	    mkdir -p "${repo}";
	    (cd "${repo}" && git init --bare)
	fi
    fi
}


git_own_checkout ()
{
    local section name
    section=$1; name=$2;

    local dest=${HOME}/repos/own/${section}/${name}
    if online ; then
	source=$(alioth_uri "${section}" "${name}")
    else
	source="${USBROOT}/${name}"
    fi
    git clone "${source}" "${dest}"
}

git_own_fixremote ()
{
    local section name
    section=$1; name=$2; shift 2;

    for remote in alioth usb ; do
	git remote remove "$remote" 2>/dev/null || :
    done

    git remote add usb "${USBROOT}/${name}"
    git remote add alioth "$(alioth_uri "${section}" "${name}")"

    local rname uri
    for extra ; do
	remote=$(echo "${extra}" |cut -d= -f1)
	uri=$(echo "${extra}"   |cut -d= -f2)
	git remote remove "${remote}" 2>/dev/null || :
	git remote add "${remote}" "${uri}"
    done
}
