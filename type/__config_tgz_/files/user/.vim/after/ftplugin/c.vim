vnoremap  <buffer> -- :!boxes -m -d c<CR>
setlocal equalprg=indent\ -linux\ -psl
let b:undo_ftplugin .= "| setl equalprg< | unmap <buffer> --"
