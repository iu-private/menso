setlocal expandtab
setlocal shiftwidth=2
setlocal softtabstop=2
setlocal textwidth=79
setlocal equalprg=brittany

nnoremap <buffer> <leader>= m'ggVG=''

au BufWrite *.hs :silent !vtags --silent 2>/dev/null &

let b:undo_ftplugin .= "| setlocal equalprg< expandtab< shiftwidth<"
let b:undo_ftplugin .= "| setlocal softtabstop< equalprg< textwidth<"
