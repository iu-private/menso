setlocal expandtab
setlocal shiftwidth=4
setlocal foldmethod=syntax
let b:undo_ftplugin .= "|setlocal expandtab< shiftwidth< foldmethod<"
