augroup filetypedetect
  au BufNewFile,BufRead *.asy setf asy
  au BufNewFile,BufRead manifest setf sh
  au BufNewFile,BufRead */.mail/*/[0123456789]* setf mail
augroup END
