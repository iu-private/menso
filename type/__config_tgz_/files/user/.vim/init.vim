call plug#begin('~/.cache/vim/plugins')
  Plug 'garbas/vim-snipmate'
  Plug 'scrooloose/nerdtree'
  Plug 'scrooloose/nerdcommenter'
  Plug 'tomtom/tlib_vim'
  Plug 'honza/vim-snippets'
  Plug 'godlygeek/tabular'
  Plug 'tpope/vim-surround'
  Plug 'neovimhaskell/haskell-vim'
call plug#end()

let g:snipMate.snippet_version = 1
let g:snips_author = "Dmitry Bogatov"
imap <TAB>   <Plug>snipMateNextOrTrigger
imap <S-Tab> <Plug>snipMateBack
imap <C-Q>   <Plug>snipMateShow

nnoremap <C-N> :NERDTreeToggle<CR>
nnoremap <C-n> :NERDTreeFocus<CR>
let g:NERDTreeChDirMode=2
let g:NERDTreeWinSize=25
