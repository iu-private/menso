cdist-type__config_tgz_(7)
==========================

NAME
----

cdist-type__config_tgz_ - install tarball with configuration files

DESCRIPTION
-----------

This type installs **config.tar.gz** archive into home directory of
user, specified as **__object_id**. Extracting that archive place all
configuration files into proper localtions, *overwriting* any previous
versions.

It would be much better to install configuration files one-by-one,
retaining more fine-grained control, but it proved to be unreasonably
slow.

REQUIRED PARAMETERS
-------------------

None.


OPTIONAL PARAMETERS
-------------------

None.

BOOLEAN PARAMETERS
------------------

extract
    Extract config.tar.gz archive into user's home directory,
    overwriting any changes without prompt.

MESSAGES
--------

None.

EXAMPLES
--------

.. code-block:: sh

   __config_tgz root

SEE ALSO
--------

:strong:`cdist-type`\ (7), :strong:`cdist-type__file`\ (7)

COPYING
-------

Copyright (C) 2018 Dmitry Bogatov. Free use of this software is granted
under the terms of the GNU General Public License version 3 or later
(GPLv3+).
