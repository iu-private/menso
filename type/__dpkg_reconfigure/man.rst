cdist-type__dpkg_reconfigure(7)
===============================

NAME
----

cdist-type__dpkg_reconfigure - reconfigure Debian package

DESCRIPTION
-----------

This type non-interactively reconfigure Debian package **$__object_id**.

REQUIRED PARAMETERS
-------------------

None.


OPTIONAL PARAMETERS
-------------------

source

   File, containing assinments to debconf variables, in format,
   expected by editor frontend. If this parameter is missing or
   is set to '-', standard input is used instead.

MESSAGES
--------

None.

EXAMPLES
--------

.. code-block:: sh

   # Configure large font in Linux tty.
   __dpkg_reconfigure console-setup <<EOF
    console-setup/fontsize="16x32"
    console-setup/store_defaults_in_debconf_db="true"
    console-setup/fontsize-fb47="16x32 (framebuffer only)"
    console-setup/codesetcode="guess"
    console-setup/fontsize-text47="16x32 (framebuffer only)"
    console-setup/charmap47="UTF-8"
    console-setup/fontface47="TerminusBold"
    console-setup/codeset47="Guess optimal character set"
EOF

SEE ALSO
--------

:strong:`cdist-type`\ (7), :strong:`cdist-type__file`\ (7),
:strong:`debconf`\ (7)

COPYING
-------

Copyright (C) 2018 Dmitry Bogatov. Free use of this software is granted
under the terms of the GNU General Public License version 3 or later
(GPLv3+).
